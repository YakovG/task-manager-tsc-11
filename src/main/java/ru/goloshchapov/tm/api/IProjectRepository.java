package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    abstract List<Project> findAll();

    abstract void add(Project project);

    abstract void remove(Project project);

    abstract void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);
}
