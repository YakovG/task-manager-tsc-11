package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add (String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);
}
